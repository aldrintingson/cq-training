package com.grundfos.wcm.impl;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.grundfos.wcm.HelloWorldService;


@Component(immediate=true, name="Hello World Service Implementation")
@Service(value = HelloWorldService.class)
public class HelloWorldServiceImpl implements HelloWorldService {
	
	private static final Logger log = LoggerFactory.getLogger(HelloWorldService.class);

	public String helloWorld(String name) {
		if(name == null){
			throw new IllegalArgumentException("Name should not be null");
		}
		
		log.info("HelloWorldServiceImpl called");
		if(StringUtils.isBlank(name) == false){
			return GREETING_START_WORD + name;
		}
		else {
			return GENERIC_GREETING;
		}
	}

}
