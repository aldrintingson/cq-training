package com.grundfos.wcm;

public interface HelloWorldService {
	
	public static final String GREETING_START_WORD = "Hello";
	public static final String GENERIC_GREETING = GREETING_START_WORD + " World!";

	String helloWorld(String name);
}