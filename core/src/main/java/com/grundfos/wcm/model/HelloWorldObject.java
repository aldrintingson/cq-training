package com.grundfos.wcm.model;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.grundfos.wcm.HelloWorldService;
import com.tocea.easycoverage.annotations.SkipAutomaticTests;

@Model(adaptables=Resource.class)
public class HelloWorldObject {
	
	private static final Logger log = LoggerFactory.getLogger(HelloWorldService.class);

	/*
	 * Match with the ./name property
	 */
	@Inject
	@Default(values="Anonymous")
    private String name;
	
	@Inject
	private HelloWorldService helloWorldService;
	
	
	private String greeting;
	
	@PostConstruct
    void initializeGreeting() {
    	log.info("Object initialized, constructing greeting.");
    	greeting = helloWorldService.helloWorld(name);
    }
    
	/**
	 * Annotated with SkipAutomaticTest because getGreeting
	 *  only returns greeting which it does not have control.
	 *  With this, EasyCoverage will skip testing this method.
	 */
	@SkipAutomaticTests 
    public String getGreeting(){
    	log.info("greeting value: " + greeting);
    	return greeting;
    }
    
    void setName(String name){
    	if(name == null){
    		throw new IllegalArgumentException("Name should not be null");
    	}
    	
    	this.name = name;
    }
    
    void setHelloWorldService(HelloWorldService helloWorldService){
    	if(helloWorldService == null){
    		throw new IllegalArgumentException("HelloWorldService should not be null");
    	}
    	
    	this.helloWorldService = helloWorldService;
    }
}