package com.grundfos.wcm.model;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.grundfos.wcm.HelloWorldService;


public class HelloWorldObjectTest {
	
	private HelloWorldObject object;
	private HelloWorldService service;
	
	private static final String MOCK_SVC_RETURN_VAL = "mock impl";

	@Before
	public void setUp() throws Exception {
		object = new HelloWorldObject();
		
		service = mock(HelloWorldService.class);
		when(service.helloWorld(anyString())).thenReturn(MOCK_SVC_RETURN_VAL);
		
		object.setHelloWorldService(service);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testInitializingGreeting() {
		object.initializeGreeting();
		
		assertEquals(MOCK_SVC_RETURN_VAL, object.getGreeting());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void whenPassingNullNameThenThrowException(){
		object.setName(null);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void whenPassingNullHelloWorldServiceThenThrowException(){
		object.setHelloWorldService(null);
	}
}
