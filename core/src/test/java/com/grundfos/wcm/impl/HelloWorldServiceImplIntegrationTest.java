package com.grundfos.wcm.impl;

import static org.junit.Assert.*;

import static org.ops4j.pax.exam.CoreOptions.*;

import javax.inject.Inject;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.Configuration;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.PaxExam;
import org.ops4j.pax.exam.spi.reactors.ExamReactorStrategy;
import org.ops4j.pax.exam.spi.reactors.PerMethod;
import org.osgi.framework.BundleContext;

import com.grundfos.wcm.HelloWorldService;

@RunWith(PaxExam.class)
@ExamReactorStrategy(PerMethod.class)
public class HelloWorldServiceImplIntegrationTest{
	
	@Inject
	private HelloWorldService service;
	
	@Inject
    private BundleContext bundleContext;
	
	@Configuration 
    public Option[] config() {
        return options(
        	mavenBundle().groupId("org.osgi").artifactId("org.osgi.core").version("4.3.0"),
        	//mavenBundle().groupId("org.apache.felix").artifactId("org.apache.felix.framework").version("3.2.2"),
        	//mavenBundle().groupId("org.apache.felix").artifactId("org.apache.felix.scr").version("1.9.6"),
        	//bundle("file:target/cq-training-core-1.0-SNAPSHOT.jar"),
        	junitBundles()
        );
    }
	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void whenSuppliedNullNameThenThrowException() {
		service.helloWorld(null);
	}
	
	//@Test
	public void whenSuppliedBlankNameThenReturnGenericGreeting() {
		assertEquals(HelloWorldService.GENERIC_GREETING, service.helloWorld(""));
	}
	
	//@Test
	public void whenSuppliedNonBlankNameThenReturnCustomGreeting() {
		String name = "test name";
		assertEquals(HelloWorldService.GREETING_START_WORD + " " + name, service.helloWorld(name));
	}
}
