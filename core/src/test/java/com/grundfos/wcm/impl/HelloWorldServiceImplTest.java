package com.grundfos.wcm.impl;

import static org.junit.Assert.*;

import org.junit.Test;

import com.grundfos.wcm.HelloWorldService;

public class HelloWorldServiceImplTest {
	
	HelloWorldServiceImpl service = new HelloWorldServiceImpl();

	@Test(expected=IllegalArgumentException.class)
	public void  whenPassedNullThenThrowException() {
		service.helloWorld(null);
	}

	@Test
	public void  whenPassedBlankNameThenReturnGenericGreeting() {
		assertEquals(HelloWorldService.GENERIC_GREETING ,service.helloWorld(""));
	}
	
	@Test
	public void  whenPassedRealNameThenReturnPersonalizedGreeting() {
		String name = "aldrin";
		assertEquals(HelloWorldService.GREETING_START_WORD + name,
				service.helloWorld(name));
	}
}
