<%@page trimDirectiveWhitespaces="true" %>
<%@include file="/apps/cq-training/global.jsp"%>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="Grundfos" />
    <script type="text/javascript">$('html').addClass('js-enabled');</script>
	<cq:includeClientLib categories="cq-training.all" />
	<cq:include script="init.jsp"/>
</head>