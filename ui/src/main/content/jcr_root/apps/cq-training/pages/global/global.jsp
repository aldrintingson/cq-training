<%@page trimDirectiveWhitespaces="true"%>
<%@include file="/apps/cq-training/global.jsp"%>
<!DOCTYPE html>
<html lang="${language}">
<%@page session="false"
            contentType="text/html; charset=utf-8"
            import="com.day.cq.commons.Doctype,
                    com.day.cq.wcm.api.WCMMode,
                    com.day.cq.wcm.foundation.ELEvaluator" %><%
    // read the redirect target from the 'page properties' and perform the
    // redirect if WCM is disabled.
    String location = properties.get("redirectTarget", "");
    // resolve variables in path
    location = ELEvaluator.evaluate(location, slingRequest, pageContext);
    if (WCMMode.fromRequest(request) == WCMMode.DISABLED && location.length() > 0) {
        // check for recursion
        if (!location.equals(currentPage.getPath())) {
            response.sendRedirect(slingRequest.getResourceResolver().map(request,request.getContextPath() + location + ".html"));
        } else {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
        return;
    }
%>
    <cq:include script="head.jsp"/>
    <cq:include script="body.jsp"/>
</html>
